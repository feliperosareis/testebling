# README

Resolução dos desafios.

### Pré-requisitos:

* Apache
* MySql
* Editor de código (VS Code, Sublime, PHPStrom...)


### Instruções

* Startar o servidor local.
* Acessar a pasta do projeto via `localhost` (Exemplo: `https://localhost/testebling/`).
* Deverá ser exibido a página inicial que dará acesso às respostas dos desafios.