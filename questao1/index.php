<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bling | Questão 1</title>


    <style>
        .container{
            width: 350px;
            margin: auto;
            padding: 15px;
        }
        label{
            display: block;
            margin-bottom: 10px;
        }
        .input-text{
            display: inline-flex;
            height: 30px;
        }
        .ctn-flex{
            display: flex;
            margin-top: 10px;
        }
        .btn-add{
            padding-left: 15px;
            padding-right: 15px;
        }
        .full-width{
            width: 100%;
        }
        button{
            height: 35px;
        }
        .rotate{
            width: 60px;
        }
    </style>

</head>
<body>

        <div class="container">
            <h2>Questão 1</h2>

            <div>
                <form action="exec1.php/?add" method="post">
                    <label class="full-width">Informe um dado para o array:</label>
                    <input type="text" name="item" class="input-text" id="in" autofocus required>
                    <button class="btn-add">+</button>
                </form>
            </div>

            <div class="display">
                <p>[<span id="array"><?php echo isset($_SESSION['q1_array']) ? $_SESSION['q1_array'] : "Aqui será exibido o array" ?></span>]</p>
            </div>

            <form action="exec1.php/?turn" method="post">
                <div>
                    <span>Quantas casas deseja rotacionar?</span>
                    <input type="number" name="step" class="input-text rotate" required value="1" min="1" max="<?php echo $_SESSION['max'] ?>">
                </div>
                <br>
                <div>
                    <span>Rotacionar para:</span>
                    <input type="radio" name="direction" value="L" checked> Esquerda
                    <input type="radio" name="direction" value="R"> Direita
                </div>
                <br>
                <button>Rotacionar &#8635;</button>
            </form>
            
            <br>
            <a href="exec1.php/?restart">Limpar</a>

            <br><br>
            <a href="/bling/questao2">Próximo</a>

        </div>
    
</body>
</html>