<?php
session_start();
$referer = $_SERVER['HTTP_REFERER'];


if (isset($_GET['restart']))
{
    clear();
}
else if (isset($_GET['add']))
{
    add();
}
else if (isset($_GET['turn']))
{
    turn();
}

function add()
{
    $referer = $_SERVER['HTTP_REFERER'];
    $post = $_POST['item'];
    
    
    if ( !isset($_SESSION['q1_array']) )
    {
        $_SESSION['q1_array'] = $post;
    }
    else
    {
        $tmp = $_SESSION['q1_array'] . ', ' . $_POST['item'];
        $_SESSION['q1_array'] = $tmp;
    }

    $size = count(explode(', ', $_SESSION['q1_array']));
    $_SESSION['q1_max'] = $size;
    
    header("Location:" . $referer);
}


function clear()
{
    $referer = $_SERVER['HTTP_REFERER'];
    $_SESSION['q1_array'] = null;
    $_SESSION['q1_max'] = 1;

    header("Location:" . $referer);

}

function turn()
{
    $referer = $_SERVER['HTTP_REFERER'];

    $step = $_POST['step'];
    $way = $_POST['direction'];

    $arr = explode(',', $_SESSION['q1_array']);

    $newArr = [];

    $idx = 0;
    
    if ($way == "L")
    {
        $i = 0;

        while ( ($i + $step) < count($arr) )
        {
            $newArr[$idx] = $arr[$i + $step];
            $i++;
            $idx++;
        }
    
        $next = count($newArr) + 1;
        $j = 0;
        while ( $j < $step )
        {
            $newArr[$next] = $arr[$j];
            $next++;
            $j++;
        }
    }
    else
    {
        $tmp = $remain = count($arr) - $step;

        while ( ($tmp) < count($arr) )
        {
            $newArr[$idx] = $arr[$tmp];
            $idx++;
            $tmp++;
        }

        $j = 0;
        while ( $j < $remain )
        {
            $newArr[$idx] = $arr[$j];
            $j++;
            $idx++;
        }
        
    }

    $_SESSION['q1_array'] = implode(', ', $newArr);
    
    header("Location:" . $referer);
}

