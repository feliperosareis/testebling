<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exame Técnico | Bling</title>

    <style>
        * {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        .container{
            width: 400px;
            padding: 30px;
        }
        ul li{
            padding: 5px 0;
        }
        .submenu{
            margin-left: 20px;
        }
        a{
            text-decoration: none;
        }
    </style>
</head>
<body>

    <div class="container">
        <h3>Exame Técnico | Bling</h3>
        <br><br>
        <p><a href="/bling/assets/Teste_DEV _ GUPY.pdf" target="_blank">Enunciado da prova.</a> &#9997;</p>
        <br><br>

        <p>Respostas das questões:</p>
        <br>

        <ul class="menu">
            <li>- <a href="/bling/questao1">Questão 1</a></li>
            <li>- <a href="/bling/questao2">Questão 2</a></li>
            <li>- <a href="/bling/questao3">Questão 3</a></li>
            <li>- <a href="/bling/questao4">Questão 4</a></li>
            <li>- <a href="/bling/questao5">Questão 5</a></li>
            <li>- <a href="/bling/questao6">Questão 6</a></li>
            <li>- <a href="/bling/questao7">Questão 7</a></li>
            <li>- <a href="/bling/questao8">Questão 8</a></li>
            <li>- Questão 9
                <ul class="submenu">
                    <li>- 9.1 <a href="/bling/assets/Cardinalidades.png" target="_blank">Cardinalidades</a></li>
                    <li>- 9.2 <a href="/bling/assets/DDL.txt" target="_blank">DDL</a></li>
                    <li>- 9.3 <a href="/bling/assets/SQL.txt" target="_blank">SQL</a></li>
                </ul>
            </li>
        </ul>
        


    </div>
    
</body>
</html>