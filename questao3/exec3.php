<?php

session_start();
$referer = $_SERVER['HTTP_REFERER'];


if (isset($_GET['restart']))
{
    clear();
}
else if (isset($_GET['add']))
{
    add();
}
else if (isset($_GET['turn']))
{
    turn();
}

function add()
{
    $referer = $_SERVER['HTTP_REFERER'];
    $begin = $_POST['begin'];
    $end = $_POST['end'];
    
    $diff = strtotime($end) - strtotime($begin);
    $days = floor($diff / (60 * 60 * 24)); 

    $_SESSION['q3_days'] = $days;
    
    header("Location:" . $referer);
}


function clear()
{
    $referer = $_SERVER['HTTP_REFERER'];
    $_SESSION['q2_array'] = null;

    header("Location:" . $referer);

}

