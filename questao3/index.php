<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Questão 3</title>

    <style>
        .container{
            width: 400px;
            margin: auto;
            padding: 15px;
        }
        label{
            display: block;
            margin-bottom: 10px;
        }
        .input-text{
            display: inline-flex;
            height: 30px;
        }
        .ctn-flex{
            display: flex;
            margin-top: 10px;
        }
        .btn-add{
            padding-left: 15px;
            padding-right: 15px;
        }
        .full-width{
            width: 100%;
        }
        button{
            height: 35px;
        }
    </style>
</head>
<body>
    
    <div class="container">
    
        <h2>Questão 3</h2>

        <div>
            <h4>Informe as datas para saber o tempo em dias entre elas.</h4>
            <form action="exec3.php/?add" method="post">
                <label class="full-width">Data Inicial</label>
                <input type="date" name="begin" class="input-text" autofocus required>
                <br><br>
                <label class="full-width">Data Final</label>
                <input type="date" name="end" class="input-text" required>
                <br>
                <br>
                <button class="btn-add">Calcular &#8635;</button>
            </form>
        </div>
        
        <div class="display">
            <p>Tempo em dias = <span id="array"><?php echo isset($_SESSION['q3_days']) ? $_SESSION['q3_days'] : "Tempo em dias" ?></span></p>
        </div>
        
        <br>
        <a href="exec3.php/?restart">Limpar</a>
        
        <br><br>
        <a href="/bling/questao2">Anterior</a>
        &nbsp;
        <a href="/bling/questao4">Próximo</a>

    </div>


</body>
</html>