<?php

session_start();
$referer = $_SERVER['HTTP_REFERER'];


if (isset($_GET['restart']))
{
    clear();
}
else if (isset($_GET['add']))
{
    add();
}
else if (isset($_GET['turn']))
{
    turn();
}

function add()
{
    $referer = $_SERVER['HTTP_REFERER'];
    $post = $_POST['item'];
    
    
    if ( !isset($_SESSION['q2_array']) )
    {
        $_SESSION['q2_array'] = $post;
    }
    else
    {
        $tmp = $_SESSION['q2_array'] . ', ' . $_POST['item'];
        $_SESSION['q2_array'] = $tmp;
    }

    $size = count(explode(',', $_SESSION['q2_array']));
    
    header("Location:" . $referer);
}


function clear()
{
    $referer = $_SERVER['HTTP_REFERER'];
    $_SESSION['q2_array'] = null;

    header("Location:" . $referer);

}

function turn()
{
    $referer = $_SERVER['HTTP_REFERER'];


    $arr = explode(',', $_SESSION['q2_array']);
    $size = count($arr);

    $even = [];
    $odd = [];

    for ($i = 0; $i < $size; $i++)
    {
        if ( $arr[$i] % 2 )
        {
            $odd[] = $arr[$i];
        }
        else
        {
            $even[] = $arr[$i];
        }
    }

    $newArr = sortAsc($even);
    $odd = sortDesc($odd);

    $l = 0;
    $k = count($odd);
    while ($l < $k)
    {
        $newArr[] = $odd[$l];
        $l++;
    }

    $_SESSION['q2_array'] = implode(', ', $newArr);
    
    header("Location:" . $referer);
}

// BubbleSort
function sortAsc($array)
{
    for($i = 0; $i < count($array); $i++)
    {
        for($j = 0; $j < count($array) - 1; $j++)
        {
            if($array[$j] > $array[$j + 1])
            {
                $aux = $array[$j];
                $array[$j] = $array[$j + 1];
                $array[$j + 1] = $aux;
            }
        }
    }

    return $array;
}

// BubbleSort
function sortDesc($array)
{
    for($i = count($array) - 1; $i > 0 ; $i--)
    {
        for($j = count($array) - 1; $j > 0; $j--)
        {
            if($array[$j] > $array[$j - 1])
            {
                $aux = $array[$j];
                $array[$j] = $array[$j - 1];
                $array[$j - 1] = $aux;
            }
        }
    }

    return $array;
}