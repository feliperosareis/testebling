<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Questão 2</title>

    <style>
        .container{
            width: 350px;
            margin: auto;
            padding: 15px;
        }
        label{
            display: block;
            margin-bottom: 10px;
        }
        .input-text{
            display: inline-flex;
            height: 30px;
        }
        .ctn-flex{
            display: flex;
            margin-top: 10px;
        }
        .btn-add{
            padding-left: 15px;
            padding-right: 15px;
        }
        .full-width{
            width: 100%;
        }
        button{
            height: 35px;
        }
    </style>
</head>
<body>
    
    <div class="container">
    
        <h2>Questão 2</h2>

        <div>
            <form action="exec2.php/?add" method="post">
                <label class="full-width">Informe apenas números inteiros para compor o vetor:</label>
                <input type="number" name="item" class="input-text" id="in" step="1" autofocus required>
                <button class="btn-add">+</button>
            </form>
        </div>
        
        <div class="display">
            <p>[<span id="array"><?php echo isset($_SESSION['q2_array']) ? $_SESSION['q2_array'] : "Aqui será exibido o vetor" ?></span>]</p>
        </div>
        
        <a href="exec2.php/?turn">
            <button class="btn-add">Reordenar &#8635;</button>
        </a>

        <br><br>
        <a href="exec2.php/?restart">Limpar</a>
        
        <br><br>
        <a href="/bling/questao1">Anterior</a>
        &nbsp;
        <a href="/bling/questao3">Próximo</a>

    </div>


</body>
</html>