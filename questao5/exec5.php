<?php

session_start();
$referer = $_SERVER['HTTP_REFERER'];


if (isset($_GET['restart']))
{
    clear();
}
else if (isset($_GET['add']))
{
    add();
}


function add()
{
    $referer = $_SERVER['HTTP_REFERER'];
    
    $_SESSION['q5_sentence'] = $sentence = $_POST['sentence'];
    $word = $_POST['word'];

    $arrSentence = explode(" ", $sentence);

    $result = false;

    foreach($arrSentence as $part)
    {
        if ($word === $part)
        {
            $result = true;
            break;
        }
    }

    $_SESSION['q5_result'] = $result ? 'A palavra <strong><u>'.$word.'</u></strong> consta no texto' : 'A palavra não foi encontrada';
    
    header("Location:" . $referer);
}



function clear()
{
    $referer = $_SERVER['HTTP_REFERER'];
    $_SESSION['q5_sentence'] = null;

    header("Location:" . $referer);

}

function fatorial($num)
{
    $calc = 1;
    while ($num > 1){
        $calc *= $num;
        $num--;
    }

    return $calc;
}
