<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bling | Questão 5</title>


    <style>
        .container{
            width: 350px;
            margin: auto;
            padding: 15px;
        }
        label{
            display: block;
            margin-bottom: 10px;
        }
        .input-text{
            display: inline-flex;
            height: 30px;
        }
        .ctn-flex{
            display: flex;
            margin-top: 10px;
        }
        .btn-add{
            padding-left: 15px;
            padding-right: 15px;
        }
        .full-width{
            width: 100%;
        }
        button{
            height: 35px;
        }
        .rotate{
            width: 60px;
        }
    </style>

</head>
<body>

        <div class="container">
            <h2>Questão 5</h2>

            <div>
                <form action="exec5.php/?add" method="post">
                    <label class="full-width">Digite uma frase:</label>
                    <input type="text" name="sentence" class="input-text full-width" autofocus required>
                    <br><br>
                    <label class="full-width">Digite a palavra que deseja buscar no texto acima:</label>
                    <input type="text" name="word" class="input-text full-width" required>
                    <br><br>

                    <button class="btn-add">Buscar &#128269;</button>
                </form>
            </div>

            <div class="display">
                <p><?php echo isset($_SESSION['q5_sentence']) ? "Frase: ". $_SESSION['q5_sentence'] : "Aqui será exibido o texto digitado" ?></p>
                <p><?php echo isset($_SESSION['q5_result']) ? $_SESSION['q5_result'] : "" ?></p>
            </div>

            
            
            <br>
            <a href="exec5.php/?restart">Limpar</a>

            <br><br>
            <a href="/bling/questao4">Anterior</a>
            &nbsp;
            <a href="/bling/questao6">Próximo</a>


        </div>
    
</body>
</html>