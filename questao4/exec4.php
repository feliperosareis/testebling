<?php

session_start();
$referer = $_SERVER['HTTP_REFERER'];


if (isset($_GET['restart']))
{
    clear();
}
else if (isset($_GET['add']))
{
    add();
}
else if (isset($_GET['turn']))
{
    turn();
}

function add()
{
    $referer = $_SERVER['HTTP_REFERER'];
    
    // total de combinações:
    $comb = fatorial(6) / (fatorial(3) * fatorial(6 - 3));

    $idx = 0;
    $result = '';

    // Combinações possíveis:
    $array = array($_POST['num1'], $_POST['num2'], $_POST['num3'], $_POST['num4'], $_POST['num5'], $_POST['num6']);

    $sizeArray = count($array);

    for ($i = 0; $i < $sizeArray; $i++)
    {
        for ($j = $i + 1; $j < $sizeArray; $j++)
        {
            for ($k = $j + 1; $k < $sizeArray; $k++)
            {
                $result .=  ' ['. $array[$i] . $array[$j] . $array[$k] . '] ';
                $idx++;
            }
        }
    }

    $_SESSION['q4_comb'] = $comb;
    $_SESSION['q4_array'] = $result;

    
    header("Location:" . $referer);
}


function clear()
{
    $referer = $_SERVER['HTTP_REFERER'];
    $_SESSION['q4_array'] = null;

    header("Location:" . $referer);

}

function fatorial($num)
{
    $calc = 1;
    while ($num > 1){
        $calc *= $num;
        $num--;
    }

    return $calc;
}
