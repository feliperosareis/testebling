<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Questão 4</title>

    <style>
        .container{
            width: 400px;
            margin: auto;
            padding: 15px;
        }
        label{
            display: block;
            margin-bottom: 10px;
        }
        .input-text{
            display: inline-flex;
            height: 30px;
        }
        .ctn-flex{
            display: flex;
            flex-wrap: wrap;
            margin-top: 10px;
        }
        .group{
            margin: 10px 5px;
        }
        .btn-add{
            padding-left: 15px;
            padding-right: 15px;
        }
        .full-width{
            width: 100%;
        }
        button{
            height: 35px;
        }
    </style>
</head>
<body>
    
    <div class="container">
    
        <h2>Questão 4</h2>

        <div>
            <h4>Preencha os campos numéricos abaixo.</h4>
            <form action="exec4.php/?add" method="post">
                <div class="ctn-flex">
                    <div class="group">
                        <label class="full-width">Número</label>
                        <input type="number" name="num1" class="input-text" autofocus required>
                    </div>
                    <div class="group">
                        <label class="full-width">Número</label>
                        <input type="number" name="num2" class="input-text" required>
                    </div>
                    <div class="group">
                        <label class="full-width">Número</label>
                        <input type="number" name="num3" class="input-text" required>
                    </div>
                    <div class="group">
                        <label class="full-width">Número</label>
                        <input type="number" name="num4" class="input-text" required>
                    </div>
                    <div class="group">
                        <label class="full-width">Número</label>
                        <input type="number" name="num5" class="input-text" required>
                    </div>
                    <div class="group">
                        <label class="full-width">Número</label>
                        <input type="number" name="num6" class="input-text" required>
                    </div>
                </div>
                
                <br>
                <button class="btn-add">Calcular &#8635;</button>
            </form>
        </div>

        <br>        
        <div class="display">
            <p>Qtde de triângulos: <?php echo isset($_SESSION['q4_comb']) ? $_SESSION['q4_comb'] : "" ?></p>
            <p>Combinações possíveis:</p>
            <p><?php echo isset($_SESSION['q4_array']) ? $_SESSION['q4_array'] : "" ?></p>
        </div>
        
        <br>
        <a href="exec4.php/?restart">Limpar</a>
        
        <br><br>
        <a href="/bling/questao3">Anterior</a>
        &nbsp;
        <a href="/bling/questao5">Próximo</a>

    </div>


</body>
</html>